﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConcurrencyQueue
{
    public class ConcurrencyQueue<T>
    {
        private readonly Queue<T> _internalQueue;

        private readonly object _lock = new object();

        private readonly AutoResetEvent _syncEvent = new AutoResetEvent(false);

        public ConcurrencyQueue()
        {
            _internalQueue = new Queue<T>();
        }

        public ConcurrencyQueue(IEnumerable<T> items)
        {
            _internalQueue = new Queue<T>(items);
        }

        public void Push(T item)
        {
            lock (_lock)
            {
                _internalQueue.Enqueue(item);
                _syncEvent.Set();
            }
        }

        public T Pop()
        {
            while (_internalQueue.Count == 0)
            {
                _syncEvent.WaitOne();
            }

            lock (_lock)
            {
                T item = _internalQueue.Dequeue();
                return item;
            }
        }
    }
}

