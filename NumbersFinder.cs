﻿using System;

namespace SumOfTwoNumbers
{
    public static class NumbersFinder
    {
        public static void PrintAllNumberPairsWhoseSumIsValue(int[] numbers, int value)
        {
            int[] sortedNumbers = numbers;
            Array.Sort(sortedNumbers);

            int left = 0,
                right = sortedNumbers.Length - 1;

            while (left < right)
            {
                int current = left + 1;

                while ((current <= right) && (sortedNumbers[left] + sortedNumbers[current] <= value))
                {
                    if (sortedNumbers[left] + sortedNumbers[current] == value)
                        Console.WriteLine("Sum of {0} and {1} is {2}", sortedNumbers[left], sortedNumbers[current], value);

                    current++;
                }

                if (sortedNumbers[left] + sortedNumbers[current] > value)
                    right = current - 1;

                left++;
            }
        }
    }
}
